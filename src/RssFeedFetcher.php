<?php

namespace Drupal\rss_embed_field;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\RequestOptions;

/**
 * A service for retrieving and caching rss feeds.
 */
class RssFeedFetcher {

  /**
   * The time the cache will expire in.
   *
   * @var int
   */
  public $cacheExpire = 86400;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $httpClientFactory;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Drupal file system helper.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The Drupal time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $timeService;

  /**
   * Constructs an RssFeedFetcher object.
   *
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   The Guzzle client.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The Drupal file system helper.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The Drupal logger factory.
   * @param \Drupal\Component\Datetime\TimeInterface $time_service
   *   The Drupal time service.
   */
  public function __construct(ClientFactory $http_client_factory, CacheBackendInterface $cache, FileSystemInterface $file_system, LoggerChannelFactoryInterface $logger_factory, TimeInterface $time_service) {
    $this->httpClientFactory = $http_client_factory;
    $this->cache = $cache;
    $this->fileSystem = $file_system;
    $this->logger = $logger_factory->get('rss_embed_field');
    $this->timeService = $time_service;
  }

  /**
   * Downloads feed data.
   *
   * @param string $source_url
   *   The URL to GET.
   *
   * @return string
   *   The XML data for the RSS feed.
   */
  public function fetch($source_url) {
    $cache_key = $this->getCacheKey($source_url);
    $cache = $this->cache->get($cache_key);

    if ($cache) {
      $cached_file = $cache->data['cached_file'];
      $result = @file_get_contents($cached_file);
      if ($result) {
        return $result;
      }
      else {
        $this->cache->delete($cache_key);
      }
    }

    $sink = $this->fileSystem->tempnam('temporary://', 'rss_embed_field');
    $sink = $this->fileSystem->realpath($sink);

    return $this->get($source_url, $sink, $cache_key);
  }

  /**
   * Performs a GET request.
   *
   * @param string $url
   *   The URL to GET.
   * @param string $sink
   *   The location where the downloaded content will be saved. This can be a
   *   resource, path or a StreamInterface object.
   * @param string $cache_key
   *   The cache key to find cached headers. Defaults to false.
   *
   * @return string
   *   A Guzzle response.
   *
   * @see \GuzzleHttp\RequestOptions
   */
  protected function get($url, $sink, $cache_key) {
    $result = NULL;

    if (!$result) {
      try {
        $request = new Request('GET', $url);
        $options = [
          RequestOptions::SINK => $sink,
          RequestOptions::HEADERS => ['User-Agent' => 'FeedFetcher-Google'],
        ];

        $response = $this->httpClientFactory->fromOptions($options)->send($request);
        $cache_expiration = $this->timeService->getRequestTime() + $this->cacheExpire;
        $this->cache->set($cache_key, ['cached_file' => $sink], $cache_expiration);
        $result = (string) $response->getBody();
      }
      catch (RequestException $e) {
        $args = ['%site' => $url, '%error' => $e->getMessage()];
        $this->logger->warning('Unable to fetch the feed from %site seems to be broken because of error "%error".', $args);
      }
    }
    return $result;
  }

  /**
   * Returns the download cache key for a given feed.
   *
   * @param string $source_url
   *   The feed url to find the cache key for.
   *
   * @return string
   *   The cache key for the feed.
   */
  protected function getCacheKey($source_url) {
    return hash('sha256', $source_url);
  }

}
