<?php

namespace Drupal\rss_embed_field;

/**
 * Provides the interface for a service that retrieves and caches rss feeds.
 */
interface RssFeedFetcherInterface {

  /**
   * Downloads feed data.
   *
   * @param string $source_url
   *   The URL to GET.
   *
   * @return string
   *   The XML data for the RSS feed.
   */
  public function fetch(string $source_url);

}
