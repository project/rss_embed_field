<?php

namespace Drupal\rss_embed_field\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Laminas\Feed\Reader\Reader;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field formatter for rss feeds.
 *
 * @FieldFormatter(
 *   id = "rss_embed_field",
 *   label = @Translation("RSS Feed"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class Rss extends FormatterBase {

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \Drupal\rss_embed_field\RssFeedFetcher
   */
  protected $fetcher;

  /**
   * The Laminas feed reader.
   *
   * @var \Laminas\Feed\Reader\Reader
   */
  protected $reader;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->logger = $container->get('logger.factory')->get('rss_embed_field');
    $instance->fetcher = $container->get('rss_embed_field.fetcher');
    $instance->reader = $container->get('feed.bridge.reader');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $elements['show_title'] = [
      '#title' => $this->t('Show title'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('show_title'),
    ];

    $elements['strip_html'] = [
      '#title' => $this->t('Remove HTML'),
      '#type' => 'checkbox',
      '#default_value' => $this->getSetting('strip_html'),
    ];

    $elements['items'] = [
      '#title' => $this->t('Items'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('items'),
      '#description' => $this->t('Amount of items to display. <em>0</em> to display all items.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'items' => 10,
      'show_title' => TRUE,
      'strip_html' => TRUE,
    ] + parent::defaultSettings();

  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $amount = $this->getSetting('items') == 0 ? $this->t('all') : $this->getSetting('items');
    $summary = [];
    $summary[] = $this->getSetting('show_title') ? $this->t('Show title') : $this->t('Hide title');
    $summary[] = $this->getSetting('strip_html') ? $this->t('Remove HTML') : $this->t('Allow HTML');
    $summary[] = $this->t('Display @amount items', ['@amount' => $amount]);
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    // Set our bridge extension manager to Laminas Feed.
    Reader::setExtensionManager($this->reader);

    $elements = [];
    foreach ($items as $delta => $item) {

      try {
        // Try to load the feed.
        $feed_response = $this->fetcher->fetch($item->uri);
        $channel = Reader::importString($feed_response);
      }
      catch (\Exception $e) {
        $this->logger->warning('The feed from %site seems to be broken because of error "%error".', [
          '%site' => $item->uri,
          '%error' => $e->getMessage(),
        ]);
        $channel = FALSE;
      }

      if ($channel !== FALSE) {
        $feed_items = [];
        $allowed_tags = array_merge(Xss::getHtmlTagList(), [
          'h1',
          'h2',
          'h3',
          'h4',
          'h5',
          'h6',
          'img',
          'hr',
          'p',
        ]);

        // Build array from response.
        foreach ($channel as $feed_item) {
          $feed_items[] = [
            'title' => $this->sanitizeInput($feed_item->getTitle(), $allowed_tags),
            'description' => $this->sanitizeInput($feed_item->getDescription(), $allowed_tags),
            'link' => $feed_item->getLink(),
          ];
        }

        if ($this->getSetting('items') != 0) {
          $feed_items = array_slice($feed_items, 0, $this->getSetting('items'));
        }

        $element = [
          '#theme' => 'rss_feed',
          '#title' => $this->getSetting('show_title') ? $channel->getTitle() : NULL,
          '#items' => $feed_items,
          '#cache' => [
            'max-age' => $this->fetcher->cacheExpire,
          ],
        ];

        $elements[$delta] = $element;
      }
    }
    return $elements;
  }

  /**
   * Sanitize the data from the RSS feed before display.
   *
   * @param string $html
   *   The HTML to parse.
   * @param string $allowed_tags
   *   The tags to allow in the parsed HTML.
   *
   * @return string
   *   The sanitized output.
   */
  protected function sanitizeInput($html, $allowed_tags) {
    if (empty($html)) {
      return '';
    }
    else {
      if ($this->getSetting('strip_html')) {
        return strip_tags($html);
      }
      else {
        $html = Xss::filter($html, $allowed_tags);
        $html = Html::normalize($html);
        return Markup::create($html);
      }
    }

  }

}
