CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The RSS embed field module allows the user to integrate and setup a field
(standard Link field) within a Content type, that offers you the possibility to
display the latest Posts from a specific RSS-Feed within a node.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/rss_embed_field

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/rss_embed_field


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * RSS Field should only be installed via Composer. The zip files are provided
   for informative purposes only.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Structure > Content types > [Content type to
       edit] and add a Link field.
    3. Navigate to Manage form display and change the link's formatter to RSS
       Feed. Save.
    4. Navigate to Manage display and change the link's format to RSS Feed.
    5. Now you have to enter a Node of this Content type and type in the Link
       to the RSS Feed into the RSS-Field. After Saving the node, the latest
       posts are shown within this node.


MAINTAINERS
-----------

This module is sponsored by HUSS VERLAG (Publisher from Munich Germany) and
programmed by KEY-TEC (Drupal-Developer from Munich Germany).

 * HUSS VERLAG - https://www.huss-verlag.de/de
 * KEY-TEC - https://www.key-tec.de
 * ZenSource - https://www.zensource.cloud
